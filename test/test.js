const chai = require('chai');
const {assert} = require('chai');

//Import and use chai-http to allow chai to send requests to our server
const http = require('chai-http');
chai.use(http);

describe("api_test_suite", () => {
	it("test_api_get_users_is_running", () => {

		//.request() method is used by chai to create an http request to the given server.
		//.get("/endpoint") is used to run/access a get method route
		//.end() is used to access the response of from the route. It has an anonymous function as an argument which handles 2 objects, the error and response object which we can assert a condition with.
		chai.request('http://localhost:4000').get('/users')
		.end((err, res) => {
			//isDefined() is a an assertion that the given object/data is NOT undefined like a shortcut to notEqual(typeof(),undefined)
			assert.isDefined(res)
		})
	})

	it("test_api_get_users_returns_array", () => {
		chai.request('http://localhost:4000').get('/users')
		.end((err, res) => {
			//isArray() is a an assertion that the given data is an array.
			//res.body contains the body of the response. The data sent from res.send()
			assert.isArray(res.body)
		})
	})

})

// Create a test suite with 2 test cases:
describe("api_test_suite", () => {
	it("test_api_get_products_is_running", () => {

		chai.request('http://localhost:4000').get('/products')
		.end((err, res) => {
//2. to test if the /products endpoint is running and;			
			assert.isDefined(res)
		})
	})

	it("test_api_get_products_returns_array", () => {
		
		chai.request('http://localhost:4000').get('/products')
		.end((err, res) => {
//3. a test if the endpoint returns an array.
			assert.isArray(res.body)
		})
	})

})