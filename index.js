//Initialize a new expressjs api and use another terminal to run it with.
//Make sure to run the server in a separate terminal with npm run dev
//Run your test in another terminal with npm test
const express = require('express');

const app = express();

const PORT = 4000;

app.use(express.json());

let users = ["John","Johnson","Smith"];

app.get('/users', (req, res) => {
	return res.send(users);
});

//1. Initialize a new array of product names in index.js as products
let products = ["Soap","Powder","Liquid"];

// Create a new get method route on /products endpoint that sends the products array to the client with res.send()
app.get('/products', (req, res) => {
	return res.send(products);
});

app.listen(PORT, () => {
	console.log('Running on port ' + PORT);
})



   


